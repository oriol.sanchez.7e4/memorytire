package com.example.memorytire2

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.widget.*
import androidx.lifecycle.ViewModelProvider


class GameActivity : AppCompatActivity(), View.OnClickListener {
    // UI Viewss
    private lateinit var carta1: ImageView
    private lateinit var carta2: ImageView
    private lateinit var carta3: ImageView
    private lateinit var carta4: ImageView
    private lateinit var carta5: ImageView
    private lateinit var carta6: ImageView
    private var cartasUp = 0
    private lateinit var lastCarta: Carta
    private var ultimas2: MutableList<Carta> = ArrayList()
    private lateinit var movementsCount: TextView

    // ViewModel declaration
    private lateinit var viewModel: GameViewModel

    private lateinit var btnStartStop: ImageButton

    private var isStoped: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game)

        viewModel = ViewModelProvider(this).get(GameViewModel::class.java)

        viewModel.difficultat = intent.getIntExtra("difficulty", 1)

        btnStartStop = findViewById(R.id.pause)

        btnStartStop.setOnClickListener{
            if (isStoped){
                viewModel.chronometer.base = SystemClock.elapsedRealtime() - viewModel.time
                viewModel.chronometer.start()
                isStoped = false
            } else if (!isStoped) {
                viewModel.chronometer.stop()
                isStoped = true
                viewModel.time = SystemClock.elapsedRealtime() -  viewModel.chronometer.base
            }
        }

        carta1 = findViewById(R.id.carta1)
        carta2 = findViewById(R.id.carta2)
        carta3 = findViewById(R.id.carta3)
        carta4 = findViewById(R.id.carta4)
        carta5 = findViewById(R.id.carta5)
        carta6 = findViewById(R.id.carta6)
        viewModel.chronometer = findViewById(R.id.chronometer)
        movementsCount = findViewById(R.id.counter_movements)

        carta1.setOnClickListener(this)
        carta2.setOnClickListener(this)
        carta3.setOnClickListener(this)
        carta4.setOnClickListener(this)
        carta5.setOnClickListener(this)
        carta6.setOnClickListener(this)

        updateUI()

        viewModel.chronometer.start()
        isStoped = false
    }



    override fun onClick(v: View?) {
        if (isStoped){

        } else {
            when (v) {
                carta1 -> girarCarta(0, carta1)
                carta2 -> girarCarta(1, carta2)
                carta3 -> girarCarta(2, carta3)
                carta4 -> girarCarta(3, carta4)
                carta5 -> girarCarta(4, carta5)
                carta6 -> girarCarta(5, carta6)
            }
        }

    }


    // Funció que utilitzarem per girar la carta
    private fun girarCarta(idCarta: Int, carta: ImageView) {
        if (viewModel.cartes[idCarta].disabled){

        } else{
            viewModel.movements++
            if (cartasUp == 0) {
                if (!ultimas2.isEmpty()){
                    carta.setImageResource(viewModel.girarCarta(ultimas2[0].id, 3, ultimas2))
                    carta.setImageResource(viewModel.girarCarta(ultimas2[1].id, 3, ultimas2))
                    ultimas2.removeAll(ultimas2)
                }
                carta.setImageResource(viewModel.girarCarta(idCarta, cartasUp, ultimas2))
                lastCarta = Carta(idCarta, viewModel.cartes[idCarta].resId)
                cartasUp++
                ultimas2 += lastCarta
            }
            else if (cartasUp == 1){
                if (lastCarta.resId == viewModel.cartes[idCarta].resId){
                    ultimas2.removeAll(ultimas2)
                    carta.setImageResource(viewModel.girarCarta(idCarta, cartasUp, ultimas2))
                    viewModel.cartes[idCarta].disabled = true
                    viewModel.cartes[lastCarta.id].disabled = true
                    viewModel.time = SystemClock.elapsedRealtime() -  viewModel.chronometer.base
                    if (viewModel.difficultat == 1){
                        viewModel.score += (((viewModel.movements - 30) * -1)*10)
                    } else if(viewModel.difficultat == 2){
                        viewModel.score += (((viewModel.movements - 20) * -1)*10)
                    }
                }
                else {
                    carta.setImageResource(viewModel.girarCarta(idCarta, 2, ultimas2))
                    lastCarta = Carta(idCarta, viewModel.cartes[idCarta].resId)
                    cartasUp++
                    ultimas2 += lastCarta
                }
                cartasUp = 0
            }
            Log.w("score", " " + viewModel.score)
            movementsCount.setText(viewModel.movements.toString())
        }
        updateUI()
        if (gameDone()){
            Intent(this, ScoreActivity::class.java).also{
                it.putExtra("Score", viewModel.score)
                startActivity(it)
            }
        }
    }

    // Funció que restauarà l'estat de la UI
    private fun updateUI() {
        carta1.setImageResource(viewModel.estatCarta(0))
        carta2.setImageResource(viewModel.estatCarta(1))
        carta3.setImageResource(viewModel.estatCarta(2))
        carta4.setImageResource(viewModel.estatCarta(3))
        carta5.setImageResource(viewModel.estatCarta(4))
        carta6.setImageResource(viewModel.estatCarta(5))

    }

    private fun gameDone() : Boolean{
        var allUp = false
        var counter = 0
        for (carta in viewModel.cartes){
            if (carta.girada) counter++
            else{

            }
        }

        if (counter == 6) allUp = true
        return allUp
    }
}