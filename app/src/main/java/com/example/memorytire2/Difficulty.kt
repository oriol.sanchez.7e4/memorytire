package com.example.memorytire2

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProvider
import kotlinx.coroutines.delay
import java.time.Duration


class Difficulty : AppCompatActivity() {
    data class Difficulty(val modo: Int, var img: Int)

    lateinit var buttonEasy: Button
    lateinit var buttonMedium: Button
    lateinit var buttonHard: Button
    lateinit var imgMeme: ImageView


    private lateinit var viewModel: GameViewModel

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.difficulty)

        buttonEasy = findViewById(R.id.easy)
        buttonMedium = findViewById(R.id.medium)
        buttonHard = findViewById(R.id.hard)
        imgMeme = findViewById(R.id.imgMeme)

        viewModel = ViewModelProvider(this).get(GameViewModel::class.java)

        buttonEasy.setOnClickListener{
            imgMeme.setImageResource(viewModel.difficulty[0].img)
            mainLayout(1)
        }

        buttonMedium.setOnClickListener{
            imgMeme.setImageResource(viewModel.difficulty[1].img)
            mainLayout(2)
        }

        buttonHard.setOnClickListener{
            imgMeme.setImageResource(viewModel.difficulty[2].img)
            mainLayout(3)
        }

    }

    private fun mainLayout(modo: Int){
        Intent(this, MainActivity::class.java).also{
            it.putExtra("Mode", modo)
            startActivity(it)
        }
    }
}