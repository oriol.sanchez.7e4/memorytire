package com.example.memorytire2

import android.util.Log
import android.widget.Chronometer
import android.widget.ImageView
import androidx.lifecycle.ViewModel

class GameViewModelHard: ViewModel() {
    // Game data model
    var time = 0L
    var score = 0
    var movements = 0
    var difficultat = 0

    lateinit var chronometer: Chronometer

    var imatges = arrayOf(
        R.drawable.carta1,
        R.drawable.carta2,
        R.drawable.carta3,
        R.drawable.bugga,
        R.drawable.carta1,
        R.drawable.carta2,
        R.drawable.carta3,
        R.drawable.bugga
    )
    var cartes = mutableListOf<Carta>()
    var difficulty = arrayOf(
        Difficulty.Difficulty(1, R.drawable.estascagao),
        Difficulty.Difficulty(2, R.drawable.viejocagao),
        Difficulty.Difficulty(3, R.drawable.lascagao)
    )


    // Aquesta funció es estàndard de cada classe, i és com el constructor

    init {
        setDataModel()
    }


    // Funció que barreja les imatges de les cartes i crea l'array de Cartes
    private fun setDataModel() {
        imatges.shuffle()
        for (i in 0..7) {
            cartes.add(Carta(i, imatges[i]))
        }
    }

    // Funció que comprova l'estat de la carta, el canvia i envia
    // a la vista la imatge que s'ha de pintar
    fun girarCarta(idCarta: Int, count: Int, ultimas: MutableList<Carta>) : Int {
        if (count == 2){
            val idUlt = ultimas[0].id
            girarCarta(idUlt, 1, ultimas)
            cartes[idCarta].girada = false
            return R.drawable.back
        } else if (count == 1 && ultimas.isEmpty()){
            cartes[idCarta].girada = true
            return cartes[idCarta].resId
        } else if (count == 0){
            cartes[idCarta].girada = true
            return cartes[idCarta].resId
        } else if (count == 1 && !ultimas.isEmpty()){
            cartes[idCarta].girada = false
            return R.drawable.back
        } else{
            cartes[idCarta].girada = false
            return R.drawable.back
        }
    }

//    fun setPuntuacio() : Int{
//        return score
//    }

//    // Funció que posa l'estat del joc al mode inicial
//    fun resetEstatJoc() {
//        for (i in 0..5) {
//            cartes[i].girada = false
//        }
//    }

    // Funció que retorna l'estat actual d'una carta
    fun estatCarta(idCarta: Int): Int {
        if(cartes[idCarta].girada) return cartes[idCarta].resId
        else return R.drawable.back
    }
}

