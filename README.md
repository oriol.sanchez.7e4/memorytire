L’aplicació tindrà un Launch Screen, que en redigirà automaticament al Main Screen, i farà que poguem anar a diferents punts de l’aplicació.

Desde el Main Screen podrem elegir comença partida, triar la dificultat del joc i obtenir informació de com funciona el joc. En cas que vulguem començar partida, sense haver triat una dificultat al joc, ens sortirà un pop up per anar a aquest apartat per triar-ne una.

Dins de la pantalla de Dificultat podrem elegir 3 dificultats:
- Facil: Aquesta ens donarà una partida amb 6 cartes, les cuals puntuarant de manera positiva durant els primers 30 moviments, en cas contrari aquests punts restarant depenen el numero de intents que t’hagi costat.
- Normal: Aquest mode és molt semblant al anterior, la diferencia esta en que tens 20 moviments, en contes de 30 i segueix estan la funció de perdre punts en cas de soperar els 20 moviments
- Dificil: El mode dificil, a diferencia dels dos anteriors, tindrà 8 cartes, en contes de 6. i tindrà 30 moviments per poder puntuar de forma positiva. En cas de superar els 30 moviments, cada carta contarà menys punts

Dins de la partida trobarem un nombre definit de cartes, el cual hauras de fer parelles entre les cartes que hi ha per sumar punts, sense pasar-te amb moviments. 
Aquesta Screen tindrà la opció de ser parada amb un botó de pause. Aques pararà el temps i no deixarà fer cap canvi en les cartes.

Al selecionar dos cartes del tauler, el joc comprobarà si les dos cartes son iguals, i sumarà punts en cas que ho sigui. En cas contrari deixarà les dos cartes boca adalt, fins a proxim click del usuari. L’usuari també tindrà un contador del temps que porta, i un contador que mostri els moviments que porta en aquella partida. 

Un cop totes les cartes estan boca adalt, el joc automaticament canviarà de pantalla a la del Resultat de la partida. Aquesta et mostrarà la puntuació obtinguda en la partida, i et donarà la opció a elegir el que vols fer a continuació:

Fer una altre partida, amb la mateixa dificultat
Tornar al menú pero poder continuar interactuant amb la aplicació. Si desprès de donar-li a aquesta vols fer una altre partida, hauras de tornar a elegir una dificultat per al joc.

La puntuació de cada partida serà calculada amb la seguent formula:
<b>puntuació = ((numero de moviments - moviment limit de cada nivell) * -1 ) * 10<b>

El moviment limit de cada nivell estarà distribuit en 30 moviments per nivell fàcil i dificil. I 20 moviments pel nivell normal. En cas de sobrepasar aquest numero el farà resta punts dins del joc. Osigui, és posible queda negatiu en la partida

Dins la app trobarem un ViewModel que ens permetrà poder guardar totes les dades importants al moment en cas que volguem sortir de la aplicació, sense tancar-la, o vulguem girar el dispositiu movil.

- En el joc he implementat algunes funcionalitats extras que podem veure aqui:
- El joc implementa algunes funcionalitats extras com, para el joc en mig de la partida. Sense que sortir cap alerta, ja que al jugar li pot resultar molesta.
- Un contador de temps dins la partida, per poder veure el temps que portem en cada una d’elles

Tota la aplicació conta amb un mode landscape, per a poder utilitzar la aplicació en mode horitzontal.
