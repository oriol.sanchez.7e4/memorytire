package com.example.memorytire2

data class Carta(val id: Int, val resId: Int, var girada: Boolean = false, var disabled: Boolean = false)