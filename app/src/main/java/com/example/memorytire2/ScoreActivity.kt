package com.example.memorytire2

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class ScoreActivity : AppCompatActivity() {
    lateinit var back_play: Button
    lateinit var back_menu: Button
    lateinit var scoreText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.score)

        val score = intent.getIntExtra("Score", 0)
        val difficulty = intent.getIntExtra("difficulty", 0)

        back_play = findViewById(R.id.back_play)
        back_menu = findViewById(R.id.back_menu)
        scoreText = findViewById(R.id.scoreText)

        back_play.setOnClickListener{
            if (difficulty == 3){
                val intent = Intent(this, GameActivityHard::class.java)
                startActivity(intent)
            } else{
                val intent = Intent(this, GameActivity::class.java)
                startActivity(intent)
            }
        }

        back_menu.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            setContentView(R.layout.menu)
        }

        scoreText.setText("SCORE:\n\n ${score} puntos")
    }
}