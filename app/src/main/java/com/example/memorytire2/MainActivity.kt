package com.example.memorytire2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    lateinit var buttonPlay: Button
    lateinit var buttonDifficulty: Button
    lateinit var buttonHelp: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu)

        val mode = intent.getIntExtra("Mode", 0)

        buttonPlay = findViewById(R.id.play)
        buttonDifficulty = findViewById(R.id.dificultat)
        buttonHelp = findViewById(R.id.ajuda)

        buttonPlay.setOnClickListener{
            Log.w("Mode", "" + mode)
            if (mode == 0){
                val intent = Intent(this, PopUpWindow::class.java)
                intent.putExtra("popuptitle", "Difficulty")
                intent.putExtra("popuptext", "Choose your difficulty!")
                intent.putExtra("popupbtn", "OK")
                intent.putExtra("darkstatusbar", false)
                startActivity(intent)

            } else if (mode == 1) {
                Intent(this, GameActivity::class.java).also {
                    intent.putExtra("difficulty", 1)
                    startActivity(it)
                }
            } else if (mode == 2) {
                Intent(this, GameActivity::class.java).also {
                    intent.putExtra("difficulty", 2)
                    startActivity(it)
                }
            } else if (mode == 3) {
                Intent(this, GameActivityHard::class.java).also {
                    intent.putExtra("difficulty", 3)
                    startActivity(it)
                }
            }
        }

        buttonDifficulty.setOnClickListener{
            val intent = Intent(this, Difficulty::class.java)
            startActivity(intent)
        }

        buttonHelp.setOnClickListener{
            val toast = Toast.makeText(applicationContext, "El juego consiste en girar cartas y encontrar las parejas", Toast.LENGTH_SHORT)
            toast.show()
        }

    }
}